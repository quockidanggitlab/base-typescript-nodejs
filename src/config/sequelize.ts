import { Sequelize } from 'sequelize';
import environment from './environment';

const db = String(environment.DATABASE);
const password = String(environment.PASSWORD);
const username = String(environment.USERNAME);

export const sequelize = new Sequelize(db, username, password, {
  dialect: "mysql",
  port: 3306,
});
sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.')
  })
  .catch( (err: any) => {
    console.error('Unable to connect to the database:', err)
  })