import db from '../../models/index'

export default class UserController {
    test(req: any, res: any)
    {   
        return db.User.create({
            name: 'abc',
            email: 'abc@gmail.com',
            password: 'mypassword'
        })
        .then((user: any) => {
            return res.status(200).send({success: true, data: user, message: 'Congrats! You have Successfull test database'})
        })
        .catch((err: any) => {
             return res.status(500).send({success: false, data: err})
        });
    }
}