import express from 'express';
import { Express } from 'express';
import { json, urlencoded } from 'body-parser';
import cors from 'cors';
import environment from './config/environment';
import * as routes from './routes/index'

const PORT: number = environment.PORT || 3000

export class App {

    private app: Express;

    private options: cors.CorsOptions = {
        allowedHeaders: ["Origin", "X-Requested-With", "Content-Type", "Accept", "X-Access-Token"],
        credentials: true,
        methods: "GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE",
        preflightContinue: false,
        optionsSuccessStatus: 200
    };
    constructor() {
        this.app = express();

        // Express middleware
        this.app.use(cors(this.options));
        this.app.use(urlencoded({ extended: true }));
        this.app.use(json());

        this.app.listen(PORT, () => {
            console.log('info', '--> Server successfully started at port %d', PORT)
        });
        routes.initRoutes(this.app)
    }

    getApp() {
        return this.app;
    }
}

new App();