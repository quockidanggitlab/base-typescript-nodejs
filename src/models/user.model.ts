module.exports = function(sequelize: any, DataTypes: any) {
  const User = sequelize.define('User', {
    id: {
      allowNull: false,
      type: DataTypes.BIGINT,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING
    },
    email: {
      allowNull: false,
      type: DataTypes.STRING
    },
    password: {
      allowNull: false,
      type: DataTypes.STRING,
      validate: {
        notEmpty: true,
        len: [6, 100]
      }
    }
  }, 
 {
    indexes: [{unique: true, fields: ['email']}],
    timestamps: false,
    freezeTableName: true,
    tableName: 'users'
  })

  User.beforeSave((user: any, options: any) => {
   
  });
  
  return User;
}