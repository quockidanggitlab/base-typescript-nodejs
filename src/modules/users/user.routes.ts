import UsersController from './user.controller';

export function initRoutes (app: any, router: any) {
    console.log('info', '--> Initialisations des routes');
    let userRoute = router;
    const userController = new UsersController();

    userRoute.get('/api/v1/test', userController.test);
    return userRoute;
}