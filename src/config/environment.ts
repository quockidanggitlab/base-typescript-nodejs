require('dotenv').config()
export default {
    DATABASE:  process.env.DATABASE,
    USERNAME: process.env.USERNAME,
    PASSWORD: process.env.PASSWORD,
    SECRET:  process.env.SECRET,
    PORT: 3000
}
 