import { Express, Request, Response } from 'express';
import * as userRoute from './../modules/users/user.routes'
import * as express from 'express';
export function initRoutes(app: Express) {

  app.use(userRoute.initRoutes(app, express.Router()))
  app.get('/', (req, res) => res.status(200).send({message: 'Welcome to Server!'}))
}